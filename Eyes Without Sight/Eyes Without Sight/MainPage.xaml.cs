﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Media.Capture;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.Graphics.Imaging;
using Windows.UI.Xaml.Media.Imaging;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using System.Text;
using Windows.Media.Core;
using Windows.Media.Playback;
using Microsoft.CognitiveServices.Speech;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Diagnostics;
using Windows.Storage.Pickers;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Eyes_Without_Sight
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 


    public sealed partial class MainPage : Page
    {

        private MediaPlayer mediaPlayer; /*for speech*/
        private StorageFile _file;
        public StorageFile fileCopy;
        static string subscriptionKey = "53cb20824b434f13bf9decfbc03de218";
        static string endpoint = "https://aieyes.cognitiveservices.azure.com/";
        static readonly string uriBase = endpoint + "vision/v2.1/analyze";


        public MainPage()
        {
            InitializeComponent();
            this._language.ItemsSource = RecognizeLanguage.SupportedForDescription;
            this.mediaPlayer = new MediaPlayer();
            
            Debug.WriteLine("HELLO");
        }

        /*SPEECH RECOGNITION*/
        private async void Speak_ButtonClicked(object sender, RoutedEventArgs e)
        {
            // Creates an instance of a speech config with specified subscription key and service region.
            // Replace with your own subscription key and service region (e.g., "westus").
            var config = SpeechConfig.FromSubscription("5bf0682f555e4b9ebb3a4d2ed8ce2c13", "uksouth");

            try
            {
                // Creates a speech synthesizer.
                using (var synthesizer = new SpeechSynthesizer(config, null))
                {
                    // Receive a text from TextForSynthesis text box and synthesize it to speaker.
                    using (var result = await synthesizer.SpeakTextAsync(this.TextForSynthesis.Text).ConfigureAwait(false))
                    {
                        // Checks result.
                        if (result.Reason == ResultReason.SynthesizingAudioCompleted)
                        {
                            NotifyUser($"Speech Synthesis Succeeded.", NotifyType.StatusMessage);

                            // Since native playback is not yet supported on UWP (currently only supported on Windows/Linux Desktop),
                            // use the WinRT API to play audio here as a short term solution.
                            using (var audioStream = AudioDataStream.FromResult(result))
                            {
                                // Save synthesized audio data as a wave file and use MediaPlayer to play it
                                var filePath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "outputaudio.wav");
                                await audioStream.SaveToWaveFileAsync(filePath);
                                mediaPlayer.Source = MediaSource.CreateFromStorageFile(await StorageFile.GetFileFromPathAsync(filePath));
                                mediaPlayer.Play();
                            }
                        }
                        else if (result.Reason == ResultReason.Canceled)
                        {
                            var cancellation = SpeechSynthesisCancellationDetails.FromResult(result);

                            StringBuilder sb = new StringBuilder();
                            sb.AppendLine($"CANCELED: Reason={cancellation.Reason}");
                            sb.AppendLine($"CANCELED: ErrorCode={cancellation.ErrorCode}");
                            sb.AppendLine($"CANCELED: ErrorDetails=[{cancellation.ErrorDetails}]");

                            NotifyUser(sb.ToString(), NotifyType.ErrorMessage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                NotifyUser($"{ex.ToString()}", NotifyType.ErrorMessage);
            }
        }

        private enum NotifyType
        {
            StatusMessage,
            ErrorMessage
        };

        private void NotifyUser(string strMessage, NotifyType type)
        {
            // If called from the UI thread, then update immediately.
            // Otherwise, schedule a task on the UI thread to perform the update.
            if (Dispatcher.HasThreadAccess)
            {
                //UpdateStatus(strMessage, type);
            }
            else
            {
                //var task = Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () => UpdateStatus(strMessage, type));
            }
        }

        /*CAMERA*/
        public async void openCamera()
        {
            /*CPATURE IMAGE*/
            CameraCaptureUI captureUI = new CameraCaptureUI();
            captureUI.PhotoSettings.Format = CameraCaptureUIPhotoFormat.Jpeg;
            captureUI.PhotoSettings.CroppedSizeInPixels = new Size(1920, 1080);

            StorageFile fileCopy = null;
            //var mediaDate = DateTime.Now.ToString("dd-MM-yyyy_H-mm-ss");

            try
            {
                StorageFile photo = await captureUI.CaptureFileAsync(CameraCaptureUIMode.Photo);
                fileCopy = await photo.CopyAsync(KnownFolders.PicturesLibrary, "ews.jpg", NameCollisionOption.ReplaceExisting);
            }

            catch
            {
                return;
            }

            /*DISPLAY IMAGE*/
            IRandomAccessStream stream = await fileCopy.OpenAsync(FileAccessMode.Read);
            BitmapDecoder decoder = await BitmapDecoder.CreateAsync(stream);
            SoftwareBitmap softwareBitmap = await decoder.GetSoftwareBitmapAsync();

            SoftwareBitmap softwareBitmapBGR8 = SoftwareBitmap.Convert(softwareBitmap,
            BitmapPixelFormat.Bgra8,
            BitmapAlphaMode.Premultiplied);

            SoftwareBitmapSource bitmapSource = new SoftwareBitmapSource();
            await bitmapSource.SetBitmapAsync(softwareBitmapBGR8);

            imageControl.Source = bitmapSource;

        }

        private void CapturePhotoButton_Click(object sender, RoutedEventArgs e)
        {
            openCamera();
        }


        /*FILE OPEN PICKER*/
        public async void openLocalImage()
        {
            FileOpenPicker picker = new FileOpenPicker();
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".png");

            try
            {
                _file = await picker.PickSingleFileAsync();
                StorageFolder appFolder = ApplicationData.Current.LocalFolder;
                StorageFile fileCopy = await _file.CopyAsync(appFolder, "ews.jpg", NameCollisionOption.ReplaceExisting);
                fileText.Text = fileCopy.Path;
            }

            catch
            {
                return;
            }
        }

        private void OpenImageButton_Click(object sender, RoutedEventArgs e)
        {
            openLocalImage();
        }


        /*COMPUTER VISION*/
        private void anaylseImage()
        {
            string imageFilePath = fileText.Text;
            //string imageFilePath = "C:\\users\\jason\\desktop\\f3.jpg";
            OutputText.Text="Wait a moment for the results to appear.";
            MakeAnalysisRequest(imageFilePath).Wait();
        }

        static async Task MakeAnalysisRequest(string imageFilePath)
        {
            try
            {
                HttpClient client = new HttpClient();

                Debug.WriteLine("HTTP REQUEST INITIATED");

                // Request headers.
                client.DefaultRequestHeaders.Add(
                    "Ocp-Apim-Subscription-Key", subscriptionKey);

                string requestParameters =
                    "visualFeatures=Categories,Description,Color";

                // Assemble the URI for the REST API method.
                string uri = uriBase + "?" + requestParameters;

                HttpResponseMessage response;

                Debug.WriteLine("GATHERING HTTP RESPONSE");

                // Read the contents of the specified local image
                // into a byte array.
                byte[] byteData = GetImageAsByteArray(imageFilePath);

                // Add the byte array as an octet stream to the request body.
                using (ByteArrayContent content = new ByteArrayContent(byteData))
                {
                    // This example uses the "application/octet-stream" content type.
                    // The other content types you can use are "application/json"
                    // and "multipart/form-data".
                    content.Headers.ContentType =
                        new MediaTypeHeaderValue("application/octet-stream");

                    // Asynchronously call the REST API method.
                    response = await client.PostAsync(uri, content);
                }

                Debug.WriteLine("SENDING IMAGE");

                // Asynchronously get the JSON response.
                string contentString = await response.Content.ReadAsStringAsync();

                Debug.WriteLine("GET RESPONSE");

                //WRITE
                //Create the text file to hold the data
                StorageFolder picturesLibrary = await KnownFolders.GetFolderForUserAsync(null /* current user */, KnownFolderId.PicturesLibrary);
                StorageFile imageAnalysisFile = await picturesLibrary.CreateFileAsync("analysis.txt", CreationCollisionOption.OpenIfExists);
                await FileIO.WriteTextAsync(imageAnalysisFile, contentString.ToString());
                Debug.WriteLine("TEXT FILE WRITTEN");

                //Use stream to write to the file
                var stream = await imageAnalysisFile.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite);

                using (var outputStream = stream.GetOutputStreamAt(0))
                {
                    using (var dataWriter = new DataWriter(outputStream))
                    {
                        await dataWriter.StoreAsync();
                        await outputStream.FlushAsync();
                    }
                }
                stream.Dispose();

                // Display the JSON response.
                Debug.WriteLine("\nResponse:\n\n{0}\n",
                    JToken.Parse(contentString).ToString());

                Debug.WriteLine("DISPLAY JSON RESPONSE");
            }
            catch (Exception e)
            {
                Debug.WriteLine("\n" + e.Message);
            }
        }

        /// <summary>
        /// Returns the contents of the specified file as a byte array.
        /// </summary>
        /// <param name="imageFilePath">The image file to read.</param>
        /// <returns>The byte array of the image data.</returns>
        static byte[] GetImageAsByteArray(string imageFilePath)
        {
            Debug.WriteLine("STARTING BYTE ARRAY FUNCTION");
            // Open a read-only file stream for the specified file.
                using (FileStream fileStream =
                new FileStream(imageFilePath, FileMode.Open, FileAccess.Read))

                {
                    // Read the file's contents into a byte array.
                    BinaryReader binaryReader = new BinaryReader(fileStream);
                    return binaryReader.ReadBytes((int)fileStream.Length);
                }
        }

        private void AnalyseImageButton_Click(object sender, RoutedEventArgs e)
        {
            anaylseImage();
        }

        private async void writeData()
        {
            //WRITE
            //Create the text file to hold the data
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile textFile = await storageFolder.CreateFileAsync("TEST.text", CreationCollisionOption.ReplaceExisting);
            await FileIO.AppendTextAsync(textFile, "Test successful!");

            //Use stream to write to the file
            var stream = await textFile.OpenAsync(FileAccessMode.ReadWrite);

            using (var outputStream = stream.GetOutputStreamAt(0))
            {
                using (var dataWriter = new DataWriter(outputStream))
                {
                    await dataWriter.StoreAsync();
                    await outputStream.FlushAsync();
                }
            }
            stream.Dispose(); // Or use the stream variable (see previous code snippet) with a using statement as well.*/

            //READ
            //Open the text file
            stream = await textFile.OpenAsync(FileAccessMode.ReadWrite);
            ulong size = stream.Size;

            using (var inputStream = stream.GetInputStreamAt(0))
            {
                using (var dataReader = new DataReader(inputStream))
                {
                    uint numBytesLoaded = await dataReader.LoadAsync((uint)size);
                    string fileContents = dataReader.ReadString(numBytesLoaded);

                    OutputText.Text = fileContents;
                }
            }
            stream.Dispose();
        }

        private void showResults_Click(object sender, RoutedEventArgs e)
        {
            writeData();
        }
    }
}
